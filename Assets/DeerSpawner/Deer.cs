﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deer : MonoBehaviour
{
	const string BUCKANIM = "Buck";
	public float Move_Mod = 0.02f;
    bool isMoving = true;
    bool destroyCalled = false;

    Animator m_animator;
    void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    void Update()
    {
        bool isWalkingPressed = Input.GetKey("space");
        m_animator.SetBool("Go", isWalkingPressed);
        if(isMoving)
        {
            transform.Translate(new Vector3(Move_Mod, 0, 0));
        }


        if (_countDown > 0)
        {
            _countDown -= Time.deltaTime;
            CountdownDie();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponentInParent<Arrow>() != null)
        {
            m_animator.SetTrigger(BUCKANIM);
            isMoving = false;
            if (!destroyCalled)
            {
                destroyCalled = true;
                _countDown = 2;
            }
        }
    }

    float _countDown = 0;
    void CountdownDie()
    {
        if (_countDown <= 0)
        {
            if (GetComponentInParent<PhotonView>() != null)
            {
                PhotonNetwork.Destroy(GetComponentInParent<PhotonView>().gameObject);
            } else
            {
                GameObject.Destroy(GetComponentInParent<Deer>().transform.parent.gameObject);
            }
        }
    }
}

