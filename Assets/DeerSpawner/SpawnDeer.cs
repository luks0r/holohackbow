﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDeer : MonoBehaviour {

	public float SpawnTime = 3f;
	public GameObject DeerRef;
	float timer;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		timer += Time.deltaTime;

        if (Input.GetKeyUp(KeyCode.R))
        {
            Spawn();
        }
		//if (timer >= SpawnTime) {
		//	Spawn ();
		//	timer = 0;
		//}
	}

	public void Spawn()
	{
        Vector3 playerPos = Camera.main.transform.position;
        playerPos.y = 0;

		Vector3 randomPOS = playerPos + new Vector3 (Random.Range (0, 2), 0, Random.Range (0, 2));
		Vector3 randomROT = new Vector3 (0, Random.Range (0, 360), 0);
		//Instantiate (DeerRef, randomPOS,  Quaternion.Euler(randomROT));
        if (PhotonNetwork.inRoom)
        {
            PhotonNetwork.Instantiate("PhotonDeer", randomPOS, Quaternion.Euler(randomROT), 0);
        } else
        {
            GameObject.Instantiate(DeerRef, randomPOS, Quaternion.Euler(randomROT));
        }
	}
}
