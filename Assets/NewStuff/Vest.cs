﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vest : MonoBehaviour {



    public int HealthToDisplay = 3;


    void Awake()
    {
        transform.GetChild(5).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(5).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(5).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(5).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(5).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;

        transform.GetChild(6).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(6).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(6).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(6).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(6).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        
        switch (HealthToDisplay)
        {
            case 0:
                transform.GetChild(5).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(4).GetComponent<SpriteRenderer>().enabled = true;

                transform.GetChild(6).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(4).GetComponent<SpriteRenderer>().enabled = true;
                break;
            case 1:
                transform.GetChild(5).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(5).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;

                transform.GetChild(6).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(6).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;
                break;
            case 2:
                transform.GetChild(5).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(5).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;

                transform.GetChild(6).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(6).GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;
                break;
            case 3:
                transform.GetChild(5).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(5).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(5).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;

                transform.GetChild(6).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
                transform.GetChild(6).GetChild(3).GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(6).GetChild(4).GetComponent<SpriteRenderer>().enabled = false;
                break;


        }
    }

    public void UpdateHealth(int health)
    {
        HealthToDisplay = health;
    }
}
