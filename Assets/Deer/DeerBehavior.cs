﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeerBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponentInParent<Arrow>() != null)
        {
            // Death
            Destroy(gameObject);
        }
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.GetComponentInParent<Arrow>() != null)
    //    {
    //        // Death
    //        Destroy(gameObject);
    //    }
    //}
}
