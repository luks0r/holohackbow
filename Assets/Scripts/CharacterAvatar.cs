﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAvatar : MonoBehaviour {
    public void AppendText(string text)
    {
        GameObject dc = GameObject.Find("DebugCanvas");
        if (dc != null)
        {
            dc.GetComponent<DebugCanvas>().AppendText(text);
        }
    }

    // Use this for initialization
    void Start () {
		if (GetComponent<PhotonView>() == null || GetComponent<PhotonView>().isMine)
        {
            transform.SetParent(Camera.main.transform, false);
            GameObject.Find("GameController").GetComponent<HoloManager>().handManager = GetComponentInChildren<HandManager>(true);
        }
        else
        {
            //transform.SetParent(GameSpace.instance.transform, false);
        }
    }
	
	// Update is called once per frame
	void Update () {		
	}

    public void SetBowVisible(bool isVisible)
    {
        if (GetComponent<PhotonView>() != null)
        {
            GetComponent<PhotonView>().RPC("NetworkSetBowVisible", PhotonTargets.All, isVisible);
        }
        else
        {
            NetworkSetBowVisible(isVisible);
        }
    }

    public string masterIp;

    public void BroadcastIP(string ip)
    {
        AppendText("Going to Broadcast IP");
        if (GetComponent<PhotonView>() != null)
        {
            AppendText("Sending Receive Broadcast IP");
            GetComponent<PhotonView>().RPC("ReceiveBroadcastIP", PhotonTargets.AllBuffered, ip);
        }
        else
        {
            AppendText("No Photon, don't");
            //    ReceiveBroadcastIP(ip);
        }
    }

    [PunRPC]
    void ReceiveBroadcastIP(string ip)
    {
        return;
        AppendText("Receiving broadcasted IP: " + ip);
        if (GetComponent<PhotonView>() != null && !GetComponent<PhotonView>().isMine)
        {
            AppendText("Not mine");
            masterIp = ip;

            AppendText("Requesting and getting data");
            // Request to receive data
            //GameInterface.instance.networkLogic.GetComponent<SpectatorView.GenericNetworkTransmitter>().SetServerIP(masterIp);
            //GameInterface.instance.networkLogic.GetComponent<SpectatorView.GenericNetworkTransmitter>().RequestAndGetData();
        }
    }

    public void RequestMasterBroadcast()
    {
        if (GetComponent<PhotonView>() != null)
        {
            GetComponent<PhotonView>().RPC("ReceiveRequestForMasterBroadcast", PhotonTargets.All);
        }
    }

    [PunRPC]
    void ReceiveRequestForMasterBroadcast()
    {
        AppendText("Receiving request from master");
        if (!GetComponent<PhotonView>().isMine)
        {
            // Request to receive data
            if (!string.IsNullOrEmpty(GameInterface.instance.myIP))
            {
                GameInterface.instance.BroadcastIP();
            }
        }
    }
    // This is all done with network transmitter
    //public void RequestNetworkAnchorFromServer(string serverIp)
    //{
    //    if (GetComponent<PhotonView>() != null)
    //    {
    //        GetComponent<PhotonView>().RPC("NetworkRequestReceivedForNetworkAnchor", PhotonTargets.All, serverIp);
    //    }
    //}

    //[PunRPC]
    //void NetworkRequestReceivedForNetworkAnchor(string serverIp)
    //{
    //    if (!GetComponent<PhotonView>().isMine)
    //    {
    //        if (serverIp.Equals(GameInterface.instance.myIP))
    //        {
    //             GameInterface.instance.gameObject.GetComponent<SpectatorView.GenericNetworkTransmitter>().
    //        }
    //    }
    //}

    [PunRPC]
    void NetworkSetBowVisible(bool isVisible)
    {
        //Debug.Log((GetComponent<PhotonView>().isMine ? "Me" : "Other") + ": Setting bow visible: " + isVisible);
        GetComponentInChildren<ArrowManager>().bow.SetActive(isVisible);
    }

    public void SetName(string name)
    {
        if (GetComponent<PhotonView>() != null)
        {
            GetComponent<PhotonView>().RPC("NetworkPlayerNameReceived", PhotonTargets.AllBuffered, name);
        }
    }

    [PunRPC]
    void NetworkPlayerNameReceived(string name)
    {
        GetComponent<Playerlogic>().SetName(name);
    }
}
