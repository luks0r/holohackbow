﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoloHand : MonoBehaviour
{
    public HandManager handManager;

    private bool _handIsInTappedMode = false;
    public bool handIsInTappedMode = false;

    public enum Mode
    {
        Default, // Pickup
        HoldingBow,
        CreatingBow,
        ArrowHand,
        Shield,
        ForceField
    }

    private Mode _currentMode;

    public Mode currentMode
    {
        get
        {
            return _currentMode;
        }

        set
        {
            if (handManager.debugCanvas)
            {
                handManager.debugCanvas.AppendText("Setting hand mode to " + value);

            }
            _currentMode = value;
        }
    }

    private void Update()
    {
        CheckHandTap();
        if (Input.GetKeyUp(KeyCode.A))
        {
            if (currentMode == Mode.ArrowHand)
            {
                handIsInTappedMode = !handIsInTappedMode;
            }
        }
    }

    public void CheckHandTap(bool forcedLoss = false)
    {
        if (forcedLoss)
            handIsInTappedMode = false;

        if (handIsInTappedMode != _handIsInTappedMode)
        {
            OnTapModeChange(handIsInTappedMode, forcedLoss);
            _handIsInTappedMode = handIsInTappedMode;
        }
    }

    void OnTapModeChange(bool didTap, bool forcedLoss = false)
    {
        if (handManager.debugCanvas)
            handManager.debugCanvas.AppendText("Tap Change in mode: " + currentMode + " - forced? " + forcedLoss);

        switch (currentMode)
        {
            case Mode.Default:
                if (didTap)
                {
                    handManager.OnDefaultTap(this);
                }
                // Check Colliders
                break;

            case Mode.HoldingBow:
                if (!didTap && forcedLoss)
                {
                    // Released the bow
                    handManager.OnBowRelease(forcedLoss);
                }
                break;

            case Mode.ArrowHand:
                if (didTap)
                {
                    // Create Arrow
                    handManager.arrowManager.CreateArrow(this);
                }
                else
                {
                    // Release Arrow
                    handManager.arrowManager.Fire();
                }
                break;

            case Mode.ForceField:
                break;

            case Mode.Shield:
                break;
        }
    }

    public void LostHand()
    {
        CheckHandTap(true);
    }
}