﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    private bool _isFired = false;
    public float timeToDie = 0;
    public bool isSonicArrow = false;
    public bool isFired
    {
        set
        {
            _isFired = value;
            if (_isFired)
            {
                GetComponent<ArrowHead>().enabled = true;
                timeToDie = 10;
                if (isSonicArrow)
                {
                    GetComponentInChildren<AudioSource>().enabled = true;
                }
            }
        }

        get
        {
            return _isFired;
        }
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isFired && !GetComponent<Rigidbody>().isKinematic)
        {
            transform.forward = GetComponent<Rigidbody>().velocity.normalized;
        }
        if (timeToDie > 0)
        {
            timeToDie -= Time.deltaTime;
            if (timeToDie <= 0)
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }
    }
}
