﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkCharacterGameSpace : Photon.MonoBehaviour {
    public GameObject character;
    public Vector3 correctPlayerLocalPos;
    public Quaternion correctPlayerLocalRot;
    public Vector3 correctPlayerLocalScale;
    public GameObject world;

    // Use this for initialization
    void Awake()
    {
        if (character == null)
        {
            character = this.gameObject;
        }
        if (world == null)
        {
            if (GameSpace.instance != null)
            {
                world = GameSpace.instance.gameObject;
            } else
            {
                Debug.Log("No gamespace world");
            }
        }
        Debug.Log(name + ": World: " + world);
        if (!photonView.isMine)
        {
            character.transform.parent = world.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.isMine)
        {
            character.transform.localPosition = Vector3.Lerp(character.transform.localPosition, this.correctPlayerLocalPos, Time.deltaTime * 5);
            character.transform.localRotation = Quaternion.Lerp(character.transform.localRotation, this.correctPlayerLocalRot, Time.deltaTime * 5);
            character.transform.localScale = Vector3.Lerp(character.transform.localScale, this.correctPlayerLocalScale, Time.deltaTime * 5);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //		Debug.Log ("photon serialize view");
        if (stream.isWriting)
        {
            if (world != null)
            {
                //				Debug.Log ("World transform rotation: " + world.transform.rotation);
                //				Debug.Log ("Transform rotation: " + transform.rotation);
                //				Debug.Log ("Inverse transform rotation: " + Quaternion.Inverse(transform.rotation));
                GameObject putInWorld = new GameObject("PutInWorld");
                putInWorld.transform.SetParent(character.transform, false);
                putInWorld.transform.SetParent(world.transform, true);
                stream.SendNext(putInWorld.transform.localPosition);
                stream.SendNext(putInWorld.transform.localRotation);//transform.rotation * Quaternion.Inverse (world.transform.rotation));
                stream.SendNext(putInWorld.transform.localScale);
                Destroy(putInWorld);
            }
        }
        else
        {
            Vector3 localPos = (Vector3)stream.ReceiveNext();
            Quaternion localRot = (Quaternion)stream.ReceiveNext();
            Vector3 localScale = (Vector3)stream.ReceiveNext();
            GameObject putInWorld = new GameObject("PutInWorld");
            putInWorld.transform.SetParent(world.transform, false);
            putInWorld.transform.localPosition = localPos;
            putInWorld.transform.localRotation = localRot;
            putInWorld.transform.localScale = localScale;
            putInWorld.transform.SetParent(transform.parent, true);
            correctPlayerLocalPos = putInWorld.transform.localPosition;
            correctPlayerLocalRot = putInWorld.transform.localRotation;
            correctPlayerLocalScale = putInWorld.transform.localScale;
            Destroy(putInWorld);
        }
    }
}
