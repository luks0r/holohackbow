﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowManager : MonoBehaviour {
    public GameObject bow;

    public static ArrowManager Instance;
    public GameObject arrowPrefab;
    private GameObject currentArrow;
    public GameObject stringAttachPoint;
    public GameObject arrowStartPoint;
    public GameObject stringStartPoint;
    public bool isAttached = false;

    public HoloHand bowHand;
    public HoloHand arrowHand;
    public GameObject noc;

    public bool arrowHandDown;
    public GameObject innerBow;

    // Use this for initialization
    void Awake () {
	    if (Instance == null)
            Instance = this;
	}
	
    void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }

    bool photonIsMine
    {
        get
        {
            return GetComponentInParent<PhotonView>() == null ||
                   GetComponentInParent<PhotonView>().isMine;
        }
    }

	// Update is called once per frame
	void Update ()
    {
        if (!photonIsMine)
        {
            return;
        }

        if (bowHand != null)
        {
            if (bow.transform.position != bowHand.transform.position)
            {
                bow.transform.position = Vector3.Lerp(bow.transform.position, bowHand.transform.position, 0.5f);
            }
        }

        if (currentArrow != null)
        {
            arrowHand.transform.forward = noc.transform.position - arrowHand.transform.position;

            innerBow.transform.rotation = arrowHand.transform.rotation;
            //bow.transform.up = arrowHand.transform.up;
        }
        else
        {
            //bow.transform.forward = bow.transform.position - Camera.main.transform.position;
        }


        if (isAttached)
        {
            stringAttachPoint.transform.position = arrowHand.transform.position;// stringStartPoint.transform.localPosition + new Vector3(dist, 0f, 0f);

            // if (FINGER MOTION TO FIRE ARROW)
            //      Fire();

            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    Fire();
            //}
        }
        else
        {
            stringAttachPoint.transform.position = stringStartPoint.transform.position;
        }
    }

    public void CreateArrow(HoloHand hand)
    {
        if (PhotonNetwork.inRoom)
        {
            currentArrow = PhotonNetwork.Instantiate("Arrow", Vector3.zero, Quaternion.identity, 0);
        } else
        {
            currentArrow = GameObject.Instantiate(arrowPrefab, Vector3.zero, Quaternion.identity);
        }
        currentArrow.transform.SetParent(hand.transform, false);
        //currentArrow.transform.localPosition = new Vector3(0f, 0f, 4f);
        AttachToBow();
    }

    public float magiplier = 1;
    public void Fire()
    {
        if (currentArrow != null)
        {
            currentArrow.transform.parent = null;
            Rigidbody r = currentArrow.GetComponent<Rigidbody>();
            float magnitude = (stringStartPoint.transform.position - arrowHand.transform.position).magnitude;
            //Debug.Log("Firing at magnitude: " + magnitude + " at " + magiplier + "x");
            r.isKinematic = false;
            r.velocity = currentArrow.transform.forward * magnitude * magiplier;
            r.useGravity = true;
            //Debug.Log("firing!");
            currentArrow.GetComponent<Arrow>().isFired = true;

            currentArrow = null;
            isAttached = false;
        }
    }

    public void AttachToBow()
    {
        //currentArrow.transform.parent = stringAttachPoint.transform;
        //currentArrow.transform.localPosition = arrowStartPoint.transform.localPosition;
        //currentArrow.transform.rotation = arrowStartPoint.transform.rotation;

        isAttached = true;
    }
    

    public bool SetBowHand(HoloHand hand, bool forcedLoss = false)
    {
        if (hand != null)
        {
            if (bowHand != null && bowHand != hand)
            {
                // Weird. Bow Hand is the other hand
                return false;
            }
            bowHand = hand;
            GetComponentInParent<CharacterAvatar>().SetBowVisible(true);
            bow.SetActive(true);
            bow.transform.forward = Camera.main.transform.forward;

        }
        else
        {
            if (forcedLoss)
            {
                bowHand = null;
                // Ignore the bowhand but keep around the bow
            }
            else
            {
                bowHand.currentMode = HoloHand.Mode.Default;
                if (arrowHand != null)
                {
                    arrowHand.currentMode = HoloHand.Mode.Default;
                }

                // Remove bow
                bowHand = null;
                GetComponentInParent<CharacterAvatar>().SetBowVisible(false);
                if (currentArrow != null)
                {
                    currentArrow.SetActive(false);
                    currentArrow = null;
                }
                arrowHand = null;
            }
        }
        return true;
    }
}
