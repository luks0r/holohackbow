﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideForSelf : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GetComponentInParent<PhotonView>() == null || GetComponentInParent<PhotonView>().isMine)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.enabled = false;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
