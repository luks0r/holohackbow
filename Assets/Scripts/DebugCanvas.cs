﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugCanvas : MonoBehaviour {
    public Text outputText;

    public void ClearText()
    {
        outputText.text = "";
    }
    public void AppendText(string text)
    {
        outputText.text += text + "\n";
        Debug.Log(text);
    }
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
