﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamelogic : MonoBehaviour {
    public static Gamelogic instance;

    public float GameTimer = 120f;
    public List<Playerlogic> Players;
    public bool GameRunning = false;
    public Playerlogic HighestScoringPlayer;

	// Use this for initialization
	void Start() {
		if (instance == null)
        {
            instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (GameRunning)
        {
            GameTimer -= Time.deltaTime;
            UpdateTimeDisplays();
            if (GameTimer <= 0)
            {
                GameTimer = 0f;
                GameRunning = false;
                ReportGameOver();
            }
            GetTopPlayerByScore();
        }
	}

    void UpdateTimeDisplays()
    {

    }

    void ReportGameOver()
    {

    }

    public void StartGame()
    {
        Debug.Log("Game has started!");
        GameRunning = true;
        UpdateTimeDisplays();
    }

    public void EndGame()
    {
        Debug.Log("Game has ended! " + HighestScoringPlayer + " is the winner with " + HighestScoringPlayer.Score + " points!" );
    }

    public void AddPlayerToGame(Playerlogic player)
    {
        switch (Players.Count)
        {
            case 0:
                player.Color = (Color.red);
                break;
            case 1:
                player.Color = (Color.blue);
                break;
            case 2:
                player.Color = (Color.green);
                break;
            case 3:
                player.Color = (Color.yellow);
                break;
            case 4:
                player.Color = (Color.gray);
                break;
            default:
                player.Color = (Color.white);
                break;

        }
        Players.Add(player);
        GameInterface.instance.UpdateScoreboard();
    }

    public void RemovePlayerFromGame(Playerlogic player)
    {
        Players.Remove(player);
        GameInterface.instance.UpdateScoreboard();
    }

    private void GetTopPlayerByScore()
    {
        HighestScoringPlayer = Players[0];
        foreach (Playerlogic plyr in Players)
        {
            if (plyr.Score > HighestScoringPlayer.Score)
            {
                HighestScoringPlayer = plyr;
            }
        }
    }

}
