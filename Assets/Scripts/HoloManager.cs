﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using UnityEngine.VR.WSA.Input;

public class HoloManager : MonoBehaviour {
    public HandManager handManager;
    public DebugCanvas debugCanvas;

    GestureRecognizer gestureRecognizer;
    Dictionary<InteractionSource, HoloHand> sourceLookup = new Dictionary<InteractionSource, HoloHand>();

    // Use this for initialization
    void Start () {
        // Set up gesture recognizer
        gestureRecognizer = new GestureRecognizer();
        gestureRecognizer.SetRecognizableGestures(GestureSettings.Tap | GestureSettings.Hold | GestureSettings.DoubleTap);
        gestureRecognizer.TappedEvent += MyTapEventHandler;

        InteractionManager.SourceDetected += InteractionManager_SourceDetected;
        InteractionManager.SourcePressed += InteractionManager_SourcePressed;
        InteractionManager.SourceReleased += InteractionManager_SourceReleased;
        InteractionManager.SourceUpdated += InteractionManager_SourceUpdated;
        InteractionManager.SourceLost += InteractionManager_SourceLost;
    }

    private void Awake()
    {
        if (debugCanvas)
            debugCanvas.AppendText("HoloManager Initialized");
    }

    public void OnDestroy()
    {
        gestureRecognizer.TappedEvent -= MyTapEventHandler;
        InteractionManager.SourceDetected -= InteractionManager_SourceDetected;
        InteractionManager.SourcePressed -= InteractionManager_SourcePressed;
        InteractionManager.SourceReleased -= InteractionManager_SourceReleased;
        InteractionManager.SourceUpdated -= InteractionManager_SourceUpdated;
        InteractionManager.SourceLost -= InteractionManager_SourceLost;
    }

    public void MyTapEventHandler(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        // Interesting. We can capture single and double taps
    }

    void InteractionManager_SourceDetected(InteractionSourceState hand)
    {
        if (handManager == null)
        {
            return;
        }

        if (hand.source.kind != InteractionSourceKind.Hand)
        {
            if (debugCanvas)
            {
                debugCanvas.AppendText("Different source: ");
                debugCanvas.AppendText("Type: " + hand.source.kind);
            }
            return;
        }
        //if (debugCanvas)
        //    debugCanvas.AppendText("Detected Hand source id: " + hand.source.id);
        UpdateHandPosition(hand);
    }

    void UpdateHandPosition(InteractionSourceState hand)
    {
        Vector3 position = Vector3.zero;
        hand.properties.location.TryGetPosition(out position);

        HoloHand outHand = null;
        if (sourceLookup.TryGetValue(hand.source, out outHand))
        {
            // We have the source lookup. Update the data
            outHand.transform.position = position;
        }
        else
        {
            //if (debugCanvas)
            //    debugCanvas.AppendText("Creating Hand source id: " + hand.source.id);
            // No source lookup. Create entry
            GameObject go = new GameObject("hand");
            HoloHand holoHand = go.AddComponent<HoloHand>();
            go.transform.position = position;
            holoHand.handManager = handManager;
            handManager.AddHand(holoHand);
            sourceLookup.Add(hand.source, holoHand);
        }
    }

    void InteractionManager_SourcePressed(InteractionSourceState hand)
    {
        if (handManager == null)
        {
            return;
        }
        if (hand.source.kind != InteractionSourceKind.Hand)
        {
            if (debugCanvas)
                debugCanvas.AppendText("Different source: " + hand.source.kind);
            return;
        }
        if (debugCanvas)
            debugCanvas.AppendText("Pressed Hand source id: " + hand.source.id);
        HoloHand outObject = null;
        if (sourceLookup.TryGetValue(hand.source, out outObject))
        {
            // Set hand tapped = true
            outObject.handIsInTappedMode = true;
        }
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            GameObject go = new GameObject("hand");
            HoloHand holoHand = go.AddComponent<HoloHand>();
            go.transform.position = Camera.main.transform.position;
            holoHand.handManager = handManager;
            handManager.AddHand(holoHand);
        }
    }

    void InteractionManager_SourceReleased(InteractionSourceState hand)
    {
        if (handManager == null)
        {
            return;
        }
        if (hand.source.kind != InteractionSourceKind.Hand)
        {
            if (debugCanvas)
                debugCanvas.AppendText("Different source: " + hand.source.kind);
            return;
        }
        HoloHand outObject = null;
        if (sourceLookup.TryGetValue(hand.source, out outObject))
        {
            // Set hand tapped = false
            outObject.handIsInTappedMode = false;
        }
    }

    private uint lastSourceid;
    void InteractionManager_SourceUpdated(InteractionSourceState hand)
    {
        if (handManager == null)
        {
            return;
        }
        if (hand.source.kind != InteractionSourceKind.Hand)
        {
            if (debugCanvas)
                debugCanvas.AppendText("Different source: " + hand.source.kind);
            return;
        }
        if (hand.source.id != lastSourceid)
        {
            //debugCanvas.AppendText("Updated Hand source id: " + hand.source.id);
            lastSourceid = hand.source.id;
        }
        UpdateHandPosition(hand);
    }

    void InteractionManager_SourceLost(InteractionSourceState hand)
    {
        if (handManager == null)
        {
            return;
        }
        HoloHand outHand = null;
        if (sourceLookup.TryGetValue(hand.source, out outHand))
        {
            // We have the source lookup. Make the hand untap, then destroy
            if (outHand.handIsInTappedMode)
            {
                outHand.LostHand();
            }
            handManager.RemoveHand(outHand);
            GameObject.Destroy(outHand);
            sourceLookup.Remove(hand.source);
        }
    }

}
