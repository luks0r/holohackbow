﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameInterface : MonoBehaviour {
    public static GameInterface instance;
    private List<CharacterAvatar> players = new List<CharacterAvatar>();

    public GameObject uiGameOver;
    public GameObject uiScoreboard;
    public GameObject uiVoiceSpecial;

    public GameObject uiLobbyGroup;

    public Text uiTextScoreboard;
    public Text uiTextGameStatus;
    public Text uiTextWelcomeName;
    public Text uiTextGameConfig;


    public void AppendText(string text)
    {
        GameObject dc = GameObject.Find("DebugCanvas");
        if (dc != null)
        {
            dc.GetComponent<DebugCanvas>().AppendText(text);
        }
    }

    public enum GameMode
    {
        DefaultOnBoarding,
        GameSpaceRegistration,
        GameSpaceRegistrationServer,
        GameLobby,
        ReadyToStart,
        GameStart,
        GamePlaying,
        GameEnd
    }

    public void AddPlayer(CharacterAvatar player)
    {
        players.Add(player);
    }

    public void RemovePlayer(CharacterAvatar player)
    {
        players.Remove(player);
    }

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            SetGameRegistrationMode();
        }
	}

    public GameMode currentMode;
    public string myIP;

    public void SetGameModeAsRegistrationServer()
    {
        SetGameMode(GameMode.GameSpaceRegistrationServer);
    }

    public GameObject networkLogic;

    public void SetGameMode(GameMode newMode)
    {
        GameObject gc = GameObject.Find("GameController");
        HoloManager hm = gc.GetComponent<HoloManager>();
        if (hm.debugCanvas != null)
        {
            hm.debugCanvas.AppendText("Game Mode Set To: " + newMode);
        }

        switch (newMode)
        {
            case GameMode.DefaultOnBoarding:
                uiTextGameStatus.text = "Lobby\n\nSetting up";
                break;

            case GameMode.GameSpaceRegistration:
                break;

            case GameMode.GameSpaceRegistrationServer:
                //uiTextGameStatus.text = "Registering\n\nAnchor Base";

                //// 1. Get Network IP
                //AppendText("Setting up Game Registration Server");
                //myIP = networkLogic.GetComponent<SpectatorView.GenericNetworkTransmitter>().GetNetworkIP();
                //// 2. Configure As Server
                //AppendText("Configuring as Server");
                //networkLogic.GetComponent<SpectatorView.GenericNetworkTransmitter>().ConfigureAsServer();
                //// 3. Create Anchor
                //AppendText("Attaching Anchor");
                //networkLogic.GetComponent<HoloToolkit.Unity.WorldAnchorManager>().AttachAnchor(GameSpace.instance.gameObject, "GameSpace");
                break;

            case GameMode.ReadyToStart:
                uiTextGameStatus.text = "READY TO START";
                break;

            case GameMode.GameStart:
                uiTextGameStatus.text = "Game\n\nStarting In " + Mathf.RoundToInt(Gamelogic.instance.GameTimer);
                break;

            case GameMode.GameLobby:
                uiTextGameStatus.text = "LOBBY";
                break;

            case GameMode.GamePlaying:
                uiTextGameStatus.text = "Game\n\nEnding In " + Mathf.RoundToInt(Gamelogic.instance.GameTimer);
                break;

            case GameMode.GameEnd:
                uiTextGameStatus.text = "NEXT GAME\n\nIN " + Mathf.RoundToInt(Gamelogic.instance.GameTimer);
                break;
        }
        currentMode = newMode;
    }

    public void SetGameRegistrationMode()
    {
        SetGameMode(GameMode.GameSpaceRegistration);
    }

    public void BroadcastIP()
    {
        SetGameMode(GameMode.DefaultOnBoarding);

        // 4. Broadcast IP
        AppendText("Broadcasting IP");
        Camera.main.GetComponentInChildren<CharacterAvatar>().BroadcastIP(myIP);
        AppendText("That worked");
    }

    public bool lastGameExists = false;

    public void UpdateScoreboard()
    {
        int scoreTotal = 0;
        string scoreString = "";
        foreach(Playerlogic player in Gamelogic.instance.Players)
        {
            scoreString += player.Name;
            if (player.Score > 0)
            {
                scoreString += ": " + player.Score;
                scoreTotal += player.Score;
            }
            scoreString += "\n";
        }

        string scoreboard;
        if (currentMode == GameMode.GamePlaying)
        {
            scoreboard = "Time Remaining: " + Mathf.RoundToInt(Gamelogic.instance.GameTimer);
        }
        else if (lastGameExists)
        {
            scoreboard = "Last Game Stats";
        } else
        {
            scoreboard = "Players";
        }
    }
}
