﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpaceRegistration : MonoBehaviour {

	// Use this for initialization
	void Awake () {
	}
	
	// Update is called once per frame
	void Update () {
        if (GetComponent<Renderer>().enabled)
        {
            if (GameInterface.instance.currentMode == GameInterface.GameMode.GameSpaceRegistration)
            {
                Debug.Log("Registering");
                GameSpace.instance.SetGameSpaceByVoice(gameObject);
            }
        }

    }
}
