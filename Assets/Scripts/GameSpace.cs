﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpace : MonoBehaviour {
    public static GameSpace instance;

	// Use this for initialization
	void Start () {
		if (instance == null)
        {
            instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.G))
        {
            SetGameSpaceByVoice();
        }
    }

    public void SetGameSpaceByVoice(GameObject anchor = null)
    {
        if (GameInterface.instance.currentMode != GameInterface.GameMode.GameSpaceRegistration)
        {
            return;
        }

        GameObject gc = GameObject.Find("GameController");
        if (anchor != null)
        {
            if (gc != null)
            {
                HoloManager hmg = gc.GetComponent<HoloManager>();
                if (hmg.debugCanvas != null)
                {
                    hmg.debugCanvas.AppendText("Set gamespace by anchor: " + anchor.transform.position.ToString("F3"));
                }
                transform.position = anchor.transform.position;

                Vector3 forward = anchor.transform.forward;
                forward.y = 0;
                transform.forward = forward;
                transform.rotation = Quaternion.LookRotation(forward, Vector3.up);

                GameInterface.instance.SetGameMode(GameInterface.GameMode.DefaultOnBoarding);
                return;
            }
        }

        if (gc != null)
        {
            HoloManager hmg = gc.GetComponent<HoloManager>();
            if (hmg.handManager.GetHands().Count == 2)
            {
                List<HoloHand> hands = hmg.handManager.GetHands();
                GameObject gs = new GameObject();
                gs.transform.position =
                    hands[0].transform.position +
                    (hands[1].transform.position - hands[0].transform.position)/2;
                gs.transform.up = Vector3.up;
                gs.transform.right = hands[1].transform.position - hands[0].transform.position;

                if (hmg.debugCanvas != null)
                {
                    hmg.debugCanvas.AppendText("Set gamespace by hands");
                }
                transform.position = gs.transform.position;
                transform.rotation = gs.transform.rotation;
                GameInterface.instance.SetGameMode(GameInterface.GameMode.DefaultOnBoarding);
            }
        }
    }
}
