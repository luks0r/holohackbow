﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerlogic : MonoBehaviour {

    public string Name = "Player";
    public Color Color = Color.white;
    public int Score;
    public int NumArrows;
    public int Health;
    public bool Eliminated = false;
    
	// Use this for initialization
	void Awake() {
        //Gamelogic.instance.AddPlayerToGame(this);
        ResetStats();
	}

    private void OnDestroy()
    {
        Gamelogic.instance.RemovePlayerFromGame(this);
    }

    public void SetName(string name)
    {
        Name = name;
        GameInterface.instance.UpdateScoreboard();
    }

    void ResetStats()
    {
        Score = 0;
        NumArrows = 10;
        Health = 3;
    }

    public void LoseHealth(int dmg)
    {
        Health -= dmg;
        if (Health <= 0)
        {
            Health = 0;
            Eliminated = true;
            Debug.Log(Name + " has been eliminated!");
        }
        transform.GetChild(1).GetComponent<Vest>().UpdateHealth(Health);
    }

    public bool TryFireArrow()
    {
        if (NumArrows > 0)
        {
            NumArrows--;
            return true;

        }
        else
        {
            return false;
        }

    }

    public void GainScore(int gain)
    {
        // An arrow (with a player owner) would call this when it strikes a target or an enemy player.
        Score += gain;
        Debug.Log(Name + " has scored " + gain + " points!");
    }
}
