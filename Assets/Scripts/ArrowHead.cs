﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowHead : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        GetComponent<Arrow>().isFired = false;
        GetComponentInParent<Rigidbody>().isKinematic = true;
        GetComponentInParent<Rigidbody>().useGravity = false;
        GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
        GetComponentInParent<Rigidbody>().angularVelocity = Vector3.zero;
        //Destroy(GetComponentInParent<Arrow>().gameObject, 10);
        //Debug.LogError("Collision");
    }
}
