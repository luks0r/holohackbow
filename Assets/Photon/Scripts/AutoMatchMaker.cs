﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMatchMaker : Photon.PunBehaviour {
    public DebugCanvas debugCanvas;

	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void ConnectNetwork()
    {
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    public override void OnJoinedLobby()
    {
        if (debugCanvas)
            debugCanvas.AppendText("Joined Lobby");
        base.OnJoinedLobby();
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        if (debugCanvas)
            debugCanvas.AppendText("Join Failed");
        base.OnPhotonJoinRoomFailed(codeAndMsg);
        PhotonNetwork.CreateRoom(null);
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        if (debugCanvas)
            debugCanvas.AppendText("Join Failed");
        base.OnPhotonRandomJoinFailed(codeAndMsg);
        PhotonNetwork.CreateRoom(null);
    }

    public bool isFirstToJoin = false;
    public override void OnCreatedRoom()
    {
        if (debugCanvas)
            debugCanvas.AppendText("Created Room");

        isFirstToJoin = true;

        base.OnCreatedRoom();
    }

    public override void OnJoinedRoom()
    {
        if (debugCanvas)
            debugCanvas.AppendText("Joined Room");
        base.OnJoinedRoom();

        if (isFirstToJoin)
        {
            //GetComponent<GameInterface>().SetGameMode(GameInterface.GameMode.GameSpaceRegistrationServer);
        }
        GameObject offlineAvatar = GameObject.Find("OfflineAvatar");
        if (offlineAvatar != null)
        {
            Destroy(offlineAvatar);
        }
        // Create self
        PhotonNetwork.Instantiate("CharacterAvatar", Vector3.zero, Quaternion.identity, 0);
    }
}
