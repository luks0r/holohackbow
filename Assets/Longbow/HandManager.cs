﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandManager : MonoBehaviour
{
    private List<HoloHand> hands = new List<HoloHand>();
    public ArrowManager arrowManager;

    public enum InteractionMode
    {
        BowCreation,
        Crossbow,
        ForceFieldCreation,
        LaserPickup
    }

    public InteractionMode currentMode = InteractionMode.BowCreation;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnBowRelease(bool forcedLoss)
    {
        arrowManager.SetBowHand(null, forcedLoss);
    }

    public void OnDefaultTap(HoloHand hand)
    {
        switch (currentMode)
        {
            case InteractionMode.BowCreation:
                SetHandToBow(hand);
                break;
        }
    }

    public void SetHandToBow(HoloHand hand)
    {
        arrowManager.SetBowHand(hand);
        hand.currentMode = HoloHand.Mode.HoldingBow;
        HoloHand otherHand = OtherHand(hand);
        if (otherHand != null)
        {
            // Arrow hand
            otherHand.currentMode = HoloHand.Mode.ArrowHand;
            arrowManager.arrowHand = otherHand;
        }
    }

    public HoloHand OtherHand(HoloHand hand)
    {
        foreach (HoloHand h in hands)
        {
            if (h != hand)
            {
                return h;
            }
        }
        return null;
    }

    public DebugCanvas debugCanvas;

    public void AddHand(HoloHand hand)
    {
        hands.Add(hand);
        if (debugCanvas)
            debugCanvas.AppendText("Added - Hand Total: " + hands.Count);
        HoloHand otherHand = OtherHand(hand);
        if (otherHand != null)
        {
            if (otherHand.currentMode == HoloHand.Mode.HoldingBow)
            {
                // Arrow hand
                hand.currentMode = HoloHand.Mode.ArrowHand;
                arrowManager.arrowHand = hand;
            } else if (otherHand.currentMode == HoloHand.Mode.ArrowHand)
            {
                hand.currentMode = HoloHand.Mode.HoldingBow;
                arrowManager.bowHand = hand;
            }
        } else
        {
            if (debugCanvas)
                debugCanvas.AppendText("No other hand, setting to bow");
            // No other hand. Make it a bow
            SetHandToBow(hand);
        }
    }

    public void RemoveHand(HoloHand hand)
    {
        if (arrowManager.bowHand == hand)
        {
            //arrowManager.SetBowHand(null);
            //HoloHand otherHand = OtherHand(hand);
            //if (otherHand != null)
            //{
            //    otherHand.currentMode = HoloHand.Mode.Default;
            //}
        } else if (arrowManager.arrowHand == hand)
        {
            arrowManager.arrowHand = null;
        }
        hands.Remove(hand);
        if (debugCanvas)
            debugCanvas.AppendText("Removed - Hand Total: " + hands.Count);
    }


    public List<HoloHand> GetHands()
    {
        return hands;
    }
}
