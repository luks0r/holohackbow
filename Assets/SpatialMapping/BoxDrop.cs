﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDrop : MonoBehaviour {
    public GameObject cubePrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Space))
        {
            CreateTarget();
        }
	}

    public void CreateTarget()
    {
        if (GameInterface.instance.currentMode == GameInterface.GameMode.DefaultOnBoarding ||
            GameInterface.instance.currentMode == GameInterface.GameMode.GameLobby)
        {
            Vector3 forwardOneUnit = Camera.main.transform.position + Camera.main.transform.forward;
            Vector3 towardForward = forwardOneUnit - Camera.main.transform.position;
            towardForward.y = 0;

            GameObject target = null;

            if (PhotonNetwork.inRoom)
            {
                target = PhotonNetwork.Instantiate("BreakableTarget", Camera.main.transform.position + towardForward.normalized * 2, Quaternion.identity, 0);
            } else {
                target = GameObject.Instantiate(cubePrefab, Camera.main.transform.position + towardForward.normalized * 2, Quaternion.identity);
            }
            target.transform.forward = towardForward;
            //Rigidbody rb = cube.AddComponent<Rigidbody>();
            //rb.useGravity = true;
            //rb.isKinematic = false;
            target.gameObject.SetActive(true);
        }
    }
}
