﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour {
    public bool broken = false;
    private float despawnTimer = 1f;
    public GameObject[] children;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (broken)
        {
            despawnTimer -= Time.deltaTime;
        }

        if (despawnTimer < 0f)
        {
            if (GetComponentInParent<PhotonView>() != null) {
                PhotonNetwork.Destroy(GetComponentInParent<PhotonView>().gameObject);
            } else
            {
                GameObject.Destroy(GetComponentInParent<Breakable>().transform.parent.gameObject);
            }
        }
    }

    void OnTriggerEnter()
    {

        if (!broken)
        {

            broken = true;

            Debug.Log("Target Broken!");

            GetComponentInParent<AudioSource>().enabled = true;
            foreach (Transform child in transform)
            {
                    if (child.tag == "piece")
                    {
                        child.GetComponent<MeshRenderer>().enabled = true;
                        child.GetComponent<Rigidbody>().useGravity = true;
                        child.GetComponent<Rigidbody>().isKinematic = false;
                        child.GetComponent<BoxCollider>().enabled = true;

                        child.transform.rotation = Random.rotation;
                        child.GetComponent<Rigidbody>().velocity = child.forward * 4f;

                    }
                    else
                    {
                        child.GetComponent<MeshRenderer>().enabled = false;

                    }

            }
        }
    }
}